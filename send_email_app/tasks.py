from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth.models import User
# from celery.contrib import rdb


@shared_task
def send_mail_task():
    # rdb.set_trace()
    users = User.objects.all()
    for user in users:
        subject = "Hi! Celery Testing"
        message = "This is email for testing celery."
        email_from = settings.EMAIL_HOST_USER
        recipient_list = [user.email, ]
        send_mail(subject, message, email_from, recipient_list)
        return "Mail sending done!!!!"
