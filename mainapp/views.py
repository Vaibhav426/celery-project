from django.http import HttpResponse

from .tasks import test

from send_email_app.tasks import send_mail_task
# Create your views here.


def test_view(request):
    test.delay()
    return HttpResponse("Done!!!!")


def send_mail_func(request):
    send_mail_task.delay()
    return HttpResponse("Done!!!!")
