from django.urls import path
from . import views

urlpatterns = [
    path('home', views.test_view, name="test_view"),
    path('mail', views.send_mail_func, name="send_mail")
]
